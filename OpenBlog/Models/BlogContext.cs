﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OpenBlog.Models
{
    public class BlogContext : DbContext
    {
        public DbSet<Blogpost> Posts { get; set; }
    }
}