﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OpenBlog.Models
{
    public class Blogpost
    {
        [Key]
        public int BlogpostId { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}