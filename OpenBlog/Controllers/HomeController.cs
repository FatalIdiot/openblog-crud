﻿using OpenBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenBlog.Controllers
{
    public class HomeController : Controller
    {
        BlogContext db = new BlogContext();

        public ActionResult Index()
        {
            List<Blogpost> posts = db.Posts.ToList();
            posts.Reverse();
            ViewBag.Posts = posts;

            return View();
        }

        [HttpGet]
        public ViewResult AddNew()
        {
            return View();
        }
        
        [HttpPost]
        public RedirectResult AddNew(Blogpost newPost)
        {
            db.Posts.Add(newPost);
            db.SaveChanges();

            return Redirect("/Home/Index");
        }

        public RedirectResult RemoveById(int id)
        {
            db.Posts.Remove(db.Posts.Find(id));
            db.SaveChanges();

            return Redirect("/Home/Index");
        }

        public RedirectResult RemoveAll()
        {
            foreach (var entity in db.Posts) db.Posts.Remove(entity);
            db.SaveChanges();

            return Redirect("/Home/Index");
        }
    }
}